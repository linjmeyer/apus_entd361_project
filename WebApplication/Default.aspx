﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="WebApplication._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Lin Meyer's Coins</h1>
        <p class="lead">You should insert a new coin below</p>
    </div>

    <div class="row">
        <div class="col-md-12">
            <asp:label id="userMessage" runat="server" />
                <fieldset>
                    <legend>Insert new coin</legend>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                          <asp:TextBox runat="server" class="form-control" id="submitName" placeholder="Coin Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Weight</label>
                        <div class="col-lg-10">
                          <asp:TextBox runat="server" class="form-control" id="submitWeight" placeholder="Weight" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Metal Type</label>
                        <div class="col-lg-10">
                            <asp:RadioButtonList ID="submitMetalType" runat="server">
                                <asp:ListItem>Copper</asp:ListItem>
                                <asp:ListItem>Zync</asp:ListItem>
                                <asp:ListItem>Silver</asp:ListItem>
                                <asp:ListItem>Gold</asp:ListItem>
                                <asp:ListItem>Platinum</asp:ListItem>
                                <asp:ListItem>Pallidium</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Currency</label>
                        <div class="col-lg-10">
                            <asp:DropDownList runat="server" id="submitCurrencyType" class="form-control">
                                <asp:ListItem text="USD" />
                                <asp:ListItem text="EUR" />
                                <asp:ListItem text="GPB" />
                                <asp:ListItem Text="Other" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Face Value</label>
                        <div class="col-lg-10">
                          <asp:TextBox runat="server" class="form-control" id="submitFaceValue" placeholder="Face Value" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Collector Value</label>
                        <div class="col-lg-10">
                          <asp:TextBox runat="server" class="form-control" id="submitCollectorValue" placeholder="Collector Value" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Coin Year</label>
                        <div class="col-lg-10">
                          <asp:TextBox runat="server" class="form-control" id="submitYear" placeholder="Year" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Coin is Encapsulated</label>
                        <div class="col-lg-10">
                          <asp:CheckBox runat="server" ID="submitIsEncapped"/>
                        </div>
                    </div>
                    


                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-success" />
                    </div>
                </fieldset>
        </div>
    </div>

</asp:Content>
