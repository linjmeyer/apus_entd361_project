﻿Imports LinMeyersCoinsCommon.LinMeyersCoins
Public Class _Default
    Inherits Page
    Private validCoinDataPosted = True

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Page.IsPostBack Then
            ' They are submitting, find errors
            Dim coinName As String = submitName.Text.Trim()
            Dim coinWeight As String = submitWeight.Text.Trim()
            Dim coinCurrencyString As String = submitCurrencyType.SelectedValue.Trim()
            Dim coinMetalString As String = submitMetalType.SelectedValue.Trim()
            ' Check these values for empty
            If coinName = "" Or coinWeight = "" Or coinCurrencyString = "" Or coinMetalString = "" Then
                invalidDataGiven()
            End If
            Dim coinValue As Decimal
            If Decimal.TryParse(submitCollectorValue.Text.Trim(), coinValue) = False Then
                invalidDataGiven()
            End If
            Dim coinFaceValue As Decimal
            If Decimal.TryParse(submitFaceValue.Text.Trim(), coinFaceValue) = False Then
                invalidDataGiven()
            End If
            Dim coinYear As Integer
            If Integer.TryParse(submitYear.Text.Trim(), coinYear) = False Then
                invalidDataGiven()
            End If
            ' All data parsed, check and see if we got valid data
            If validCoinDataPosted Then
                ' valid, get the enum types then build it
                Dim coinMetal = Coin.getMetalTypeForString(coinMetalString)
                Dim coinCurrency = Coin.getCurrencyTypeForString(coinCurrencyString)
                ' We did! Make the coin
                Dim newCoin = New Coin(coinYear, coinName, coinWeight, submitIsEncapped.Checked, coinFaceValue, coinValue, coinMetal, coinCurrency)
                ' Show what we did to the user
                userMessage.Text = "<div class='alert alert-success'>Coin Created! <p>" + newCoin.toString() + "</p></div>"

            End If
        End If
    End Sub

    ' Invalid data sent, show error
    Private Sub invalidDataGiven()
        Me.validCoinDataPosted = False
        userMessage.Text = "Please provide valid data"
    End Sub
End Class