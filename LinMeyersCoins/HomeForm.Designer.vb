﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HomeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ViewCollectionButton = New System.Windows.Forms.Button()
        Me.NewCoinButton = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ViewCollectionButton
        '
        Me.ViewCollectionButton.Location = New System.Drawing.Point(271, 145)
        Me.ViewCollectionButton.Margin = New System.Windows.Forms.Padding(2)
        Me.ViewCollectionButton.Name = "ViewCollectionButton"
        Me.ViewCollectionButton.Size = New System.Drawing.Size(140, 32)
        Me.ViewCollectionButton.TabIndex = 0
        Me.ViewCollectionButton.Text = "View Collection"
        Me.ViewCollectionButton.UseVisualStyleBackColor = True
        '
        'NewCoinButton
        '
        Me.NewCoinButton.Location = New System.Drawing.Point(272, 221)
        Me.NewCoinButton.Margin = New System.Windows.Forms.Padding(2)
        Me.NewCoinButton.Name = "NewCoinButton"
        Me.NewCoinButton.Size = New System.Drawing.Size(140, 32)
        Me.NewCoinButton.TabIndex = 1
        Me.NewCoinButton.Text = "New Coin"
        Me.NewCoinButton.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(272, 290)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(139, 31)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Utilities"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'HomeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 474)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.NewCoinButton)
        Me.Controls.Add(Me.ViewCollectionButton)
        Me.Name = "HomeForm"
        Me.Text = "Home"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ViewCollectionButton As Button
    Friend WithEvents NewCoinButton As Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
