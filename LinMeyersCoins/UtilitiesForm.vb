﻿Public Class UtilitiesForm
    Private ReadOnly troyOuncesToAGram = 0.0321507 ' # of ounces = 1 gram
    Private ReadOnly gramsToATroyOunce = 31.1035  ' # of grams  = 1 troy ounce
    Private ReadOnly ouncesToATroyOunce = 1.09714 ' # of ounces  = 1 troy ounce
    Private ReadOnly troyOuncesToAnOunce = 0.911458 ' # of of troy ounces = 1 ounce
    Private ReadOnly monetaryValueTroyOunceAmmounts = {0.1, 0.25, 0.5} ' # of troy ounces to convert to monetary values


    ' Fires when the Convert Button is pressed by the user
    ' Convert between TROY OUNCES and GRAMS
    Private Sub ConvertButton_Click(sender As Object, e As EventArgs) Handles ConvertButton.Click
        ' Get the string values of our textboxes
        Dim gramsAsString = GramsTextBox.Text.Trim()
        Dim troyOuncesAsString = TroyOuncesTextBox.Text.Trim()
        ' If they both have data, show an error
        If gramsAsString <> "" And troyOuncesAsString <> "" Then
            Me.tooMuchDataGiven()
        Else
            ' Cool, only one has data - but which one?  and is it a number?
            If gramsAsString <> "" Then
                Dim gramsAsDecimal As Decimal
                ' Grams is not empty, try to parse it into a Decimal
                If Decimal.TryParse(gramsAsString, gramsAsDecimal) Then
                    ' Great, we converted into a decimal!
                    ' Display it to the user
                    TroyOuncesTextBox.Text = Me.decimalToStringFormat(Me.calculateGramsToOunces(gramsAsDecimal))
                Else
                    ' Error, they didn't give a valid number!
                    Me.invalidDataGiven()
                End If
            Else
                ' Else -> gramsAsString is empty, so ouncesAsString must not be - try to parse it
                Dim troyOuncesAsDecimal As Decimal
                If Decimal.TryParse(troyOuncesAsString, troyOuncesAsDecimal) Then
                    ' Great, we converted into a decimal! Show it to the user!
                    GramsTextBox.Text = Me.decimalToStringFormat(Me.calculateTroyOuncesToGrams(troyOuncesAsDecimal))
                Else
                    ' Error, they didn't give us a valid number!
                    Me.invalidDataGiven()
                End If
            End If
        End If
    End Sub

    ' Fires when the user presses convert between OUNCES and TROY OUNCES
    Private Sub OuncesConversionButton_Click(sender As Object, e As EventArgs) Handles OuncesConversionButton.Click
        ' Get the string values of our textboxes
        Dim troyOuncesAsString = TroyOuncesTextBox.Text.Trim()
        Dim ouncesAsString = OuncesTextBox.Text.Trim()
        ' Check which one has value, if they are both empty or both have values show an error
        If ouncesAsString <> "" And troyOuncesAsString <> "" Then
            ' Both have values - show error
            Me.tooMuchDataGiven()
        Else
            ' Great, one of them is empty - figure out which one and parse into decimal
            If troyOuncesAsString <> "" Then
                ' Troy Ounces -> Ounces it is!
                ' Try to parse it
                Dim troyOuncesAsDecimal As Decimal
                If Decimal.TryParse(troyOuncesAsString, troyOuncesAsDecimal) Then
                    ' Great, we parsed!
                    OuncesTextBox.Text = Me.decimalToStringFormat(Me.calculateTroyOuncesToOunces(troyOuncesAsDecimal))
                Else
                    ' Error parsing, must be invalid data
                    Me.invalidDataGiven()
                End If
            Else
                ' Ounces -> Troy ounces then!
                ' Try to parse it
                Dim ouncesAsDecimal As Decimal
                If Decimal.TryParse(ouncesAsString, ouncesAsDecimal) Then
                    '  Great! good data - do the math
                    TroyOuncesTextBox.Text = Me.decimalToStringFormat(Me.calculateOuncesToTroyOunces(ouncesAsDecimal))
                Else
                    ' Couldn't parse, must be invalid data!
                    Me.invalidDataGiven()
                End If
            End If
        End If
    End Sub

    ' Takes a decimal, returns a formatted string with two decimal places
    Private Function decimalToStringFormat(dec As Decimal) As String
        Return Format(dec, "0.00")
    End Function

    ' Takes grams, returns ounces
    Private Function calculateGramsToOunces(grams As Decimal) As Decimal
        Return grams * Me.troyOuncesToAGram
    End Function

    ' Takes troy ounces, returns grams
    Private Function calculateTroyOuncesToGrams(troyOunces As Decimal) As Decimal
        Return troyOunces * Me.gramsToATroyOunce
    End Function

    ' Takes troy ounces, returns ounces
    Private Function calculateTroyOuncesToOunces(troyOunces As Decimal) As Decimal
        Return troyOunces * Me.ouncesToATroyOunce
    End Function

    ' Takes ounces, returns troy ounces
    Private Function calculateOuncesToTroyOunces(ounces As Decimal) As Decimal
        Return ounces * Me.troyOuncesToAnOunce
    End Function

    Private Sub invalidDataGiven()
        MessageBox.Show("Invalid data, please only enter numbers and decimals!")
    End Sub

    Private Sub tooMuchDataGiven()
        MessageBox.Show("Looks like you have filled out both textboxes.  Please clear one or both!")
    End Sub

    ' When the user clicks on Clear Values, empty all text boxes
    Private Sub ClearValuesButton_Click(sender As Object, e As EventArgs) Handles ClearValuesButton.Click
        GramsTextBox.Text = ""
        TroyOuncesTextBox.Text = ""
        OuncesTextBox.Text = ""
    End Sub

    ' Accepts a textbox and returns the decimal value
    ' if showError is true, displays an error to the user
    ' Returns 0 if an error is displays
    Private Function getTextBoxValueAsDecimal(tbox As TextBox, showError As Boolean) As Decimal
        Dim returnDecimal As Decimal = 0
        ' If we can't parse it, show an error
        ' Else If, value is negative or 0 show error
        ' Else not necessary, just return the value
        If Not Decimal.TryParse(tbox.Text, returnDecimal) Then
            Me.invalidDataGiven()
        ElseIf returnDecimal <= 0 Then
            returnDecimal = 0
            Me.invalidDataGiven() ' Negative or 0 value!
        End If
        ' Decimal will be 0, or a valid decimal # from the user
        Return returnDecimal
    End Function

    ' Accepts a Decimal $$ value and a string for the type (platinum, gold, etc)
    ' Will calculate the amount and present it to the user
    ' ASSUMES DECIMAL IS NOT 0 AND IS POSITIVE!!
    Private Sub calculateValueForTroyOunces(valueOfOne As Decimal, type As String)
        Dim messageForUser As String = ""
        For Each amountInTroyOunce In Me.monetaryValueTroyOunceAmmounts
            ' For Each of our Troy Ounce ammounts, calculate the value (for 1 Troy Ounce)
            ' Save it to our List of Decimals and present that to the user
            Dim currentCalcValue As Decimal = valueOfOne * amountInTroyOunce
            ' Cool, save the value to our string to be presented when we are done
            ' :C converts to 2 decimal places.  Pretty cool
            messageForUser += String.Format("{0:C} troy ounces of {1} = {2:C}{3}", amountInTroyOunce, type, currentCalcValue, Environment.NewLine)
        Next
        ' Present it!
        MessageBox.Show(messageForUser)
    End Sub

    ' Fires when the user clicks the "Calculate" button for Platinum -> $$ conversions
    Private Sub platinumValueButton_Click(sender As Object, e As EventArgs) Handles platinumValueButton.Click
        Dim decimalValue = Me.getTextBoxValueAsDecimal(platinumTextBox, True)
        ' We will get 0 if an error occurred, the message will already be presented
        If decimalValue <> 0 Then
            Me.calculateValueForTroyOunces(decimalValue, "platinum")
        End If
    End Sub

    ' Fires when the user clicks on on the calculate button for gold
    Private Sub goldCalcButton_Click(sender As Object, e As EventArgs) Handles goldCalcButton.Click
        Dim decimalValue = Me.getTextBoxValueAsDecimal(goldTextBox, True)
        ' We will get 0 if an error occurred, the message will already be presented
        If decimalValue <> 0 Then
            Me.calculateValueForTroyOunces(decimalValue, "gold")
        End If
    End Sub

    ' Fires when the user clicks the calc button for silver
    Private Sub silverCalcButton_Click(sender As Object, e As EventArgs) Handles silverCalcButton.Click
        Dim decimalValue = Me.getTextBoxValueAsDecimal(silverTextBox, True)
        ' We will get 0 if an error occurred, the message will already be presented
        If decimalValue <> 0 Then
            Me.calculateValueForTroyOunces(decimalValue, "silver")
        End If
    End Sub
End Class