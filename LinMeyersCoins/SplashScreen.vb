﻿Imports LinMeyersCoins.LinMeyersCoinsDBDataSetTableAdapters
Imports LinMeyersCoinsCommon.LinMeyersCoins.Coin

Public Class SplashScreen

    Private timer As New Timer() ' A timer used to track how long the window has been open
    Private timerStarted As DateTime ' A datetime used to store when the timer starts

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.TopMost = True ' Forces this program on top, unless another program calls TopMost after.

        ' Create some dummy data
        Me.CreateDummyData()
    End Sub

    ' Fires after the object is constructed and loads
    Private Sub SplashScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Point the "tick" of the timer towards our handler
        AddHandler Me.timer.Tick, AddressOf Timer_Tick
        ' Run the tick every 50 milliseconds
        Me.timer.Interval = 50
        ' Set are start time to now, and start the timer
        Me.timerStarted = DateTime.Now
        Me.timer.Start()

    End Sub

    Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' If the difference between the start time and current time is 3 seconds or more, close the window and open the main program
        Dim difference As TimeSpan = DateTime.Now.Subtract(Me.timerStarted)
        If difference.TotalSeconds >= 3 Then
            ' Lets do a nice fade out, keep the timer going and reduce this screens opacity a bit
            If Me.Opacity > 0 Then
                ' Splash is still visible - reduce that a bit
                Me.Opacity = Me.Opacity - 0.15
            Else
                ' Splash screen is gone, truly hide the window and open the new form (home)
                Dim homeScreen As New HomeForm

                homeScreen.fadeIn()
                ' Now hide the splash screen
                Me.Hide()
                ' Stop the timer, or we will just sit here and open home screens all day
                Me.timer.Stop()
            End If
        Else
            ' The program has not been running long enough, but lets debug a bit:
            System.Diagnostics.Debug.WriteLine("We have been running for {0} milliseconds", difference.TotalMilliseconds)
        End If
    End Sub

    ' While we are waiting for this to open - create some dummy coins (if in debug)
    Private Sub CreateDummyData()
        If Debugger.IsAttached Then
            Using coinTableAdapter = New CoinsTableAdapter
                ' Inserts dummy data (if debugging) to make it a bit easier on testing
                coinTableAdapter.Insert("Fake Coin", 1995, 25.0, CurrencyType.USD, MetalType.Copper, 1, 200.84, 1)
                coinTableAdapter.Insert("Dummy Coin", 1806, 1.0, CurrencyType.GBP, MetalType.Gold, 1, 7854.15, 1)
                coinTableAdapter.Insert("Lin's Coin", 1701, 0.1, CurrencyType.USD, MetalType.Palladium, 0, 478.15, 1)
                coinTableAdapter.Insert("America's Coin", 1776, 1.0, CurrencyType.USD, MetalType.Gold, 1, 215.15, 1)
                coinTableAdapter.Insert("Rachael's Coin", 2001, 5.0, CurrencyType.USD, MetalType.Copper, 1, 965.84, 1)
                coinTableAdapter.Insert("AMU's Coin", 2007, 15.0, CurrencyType.USD, MetalType.Copper, 1, 148.84, 1)
                coinTableAdapter.Insert("Bill Gates First Penny", 1975, 0.01, CurrencyType.USD, MetalType.Copper, 1, 9999999999, 1)
                coinTableAdapter.Insert("Fake Coin", 1995, 25.0, CurrencyType.USD, MetalType.Copper, 1, 200.84, 1)
                coinTableAdapter.Insert("Dummy Coin", 1806, 1.0, CurrencyType.GBP, MetalType.Gold, 1, 7854.15, 1)
                coinTableAdapter.Insert("Lin's Coin", 1701, 0.1, CurrencyType.USD, MetalType.Palladium, 0, 478.15, 1)
                coinTableAdapter.Insert("America's Coin", 1776, 1.0, CurrencyType.USD, MetalType.Gold, 1, 215.15, 1)
                coinTableAdapter.Insert("Rachael's Coin", 2001, 5.0, CurrencyType.USD, MetalType.Copper, 1, 965.84, 1)
                coinTableAdapter.Insert("AMU's Coin", 2007, 15.0, CurrencyType.USD, MetalType.Copper, 1, 148.84, 1)
                coinTableAdapter.Insert("Bill Gates First Penny", 1975, 0.01, CurrencyType.USD, MetalType.Copper, 1, 9999999999, 1)
                coinTableAdapter.Insert("Fake Coin", 1995, 25.0, CurrencyType.USD, MetalType.Copper, 1, 200.84, 1)
                coinTableAdapter.Insert("Dummy Coin", 1806, 1.0, CurrencyType.GBP, MetalType.Gold, 1, 7854.15, 1)
                coinTableAdapter.Insert("Lin's Coin", 1701, 0.1, CurrencyType.USD, MetalType.Palladium, 0, 478.15, 1)
                coinTableAdapter.Insert("America's Coin", 1776, 1.0, CurrencyType.USD, MetalType.Gold, 1, 215.15, 1)
                coinTableAdapter.Insert("Rachael's Coin", 2001, 5.0, CurrencyType.USD, MetalType.Copper, 1, 965.84, 1)
                coinTableAdapter.Insert("AMU's Coin", 2007, 15.0, CurrencyType.USD, MetalType.Copper, 1, 148.84, 1)
                coinTableAdapter.Insert("Bill Gates First Penny", 1975, 0.01, CurrencyType.USD, MetalType.Copper, 1, 9999999999, 1)
                coinTableAdapter.Insert("Fake Coin", 1995, 25.0, CurrencyType.USD, MetalType.Copper, 1, 200.84, 1)
                coinTableAdapter.Insert("Dummy Coin", 1806, 1.0, CurrencyType.GBP, MetalType.Gold, 1, 7854.15, 1)
                coinTableAdapter.Insert("Lin's Coin", 1701, 0.1, CurrencyType.USD, MetalType.Palladium, 0, 478.15, 1)
                coinTableAdapter.Insert("America's Coin", 1776, 1.0, CurrencyType.USD, MetalType.Gold, 1, 215.15, 1)
                coinTableAdapter.Insert("Rachael's Coin", 2001, 5.0, CurrencyType.USD, MetalType.Copper, 1, 965.84, 1)
                coinTableAdapter.Insert("AMU's Coin", 2007, 15.0, CurrencyType.USD, MetalType.Copper, 1, 148.84, 1)
                coinTableAdapter.Insert("Bill Gates First Penny", 1975, 0.01, CurrencyType.USD, MetalType.Copper, 1, 9999999999, 1)
            End Using
        End If
    End Sub

End Class
