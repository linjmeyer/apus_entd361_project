﻿Public Class HomeForm

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
    End Sub

    ' A method to mimic the default 'Show', but with a small fade in
    Public Sub fadeIn()
        ' Set opacity to 0 to hide the window
        Me.Opacity = 0
        ' Show the window, but it won't be visible with no opacity
        Me.Show()
        ' Loop our opacity up to a visible amount
        For newOpacity = 0.0 To 1.1 Step 0.15
            Me.Opacity = newOpacity
            Me.Refresh()
            Threading.Thread.Sleep(10)
        Next
    End Sub

    ' When this window is closed, kill the entire application
    ' This must be manual because the splash screen is technically still running
    Private Sub handleClose(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.FormClosing
        LinMeyersCoins.SplashScreen.Close()
    End Sub

    Private Sub ViewCollectionButton_Click(sender As Object, e As EventArgs) Handles ViewCollectionButton.Click
        ' Open the Collection form
        Dim collectionForm = New CollectionForm()
        collectionForm.Show()
    End Sub

    Private Sub NewCoinButton_Click(sender As Object, e As EventArgs) Handles NewCoinButton.Click
        ' Check if the form is open, if it is present it to the user.  If not create one
        If Me.isFormTypeAlreadyOpen(NewCoinForm, True) = False Then
            Dim newCoinForm As New NewCoinForm
            newCoinForm.Show()
        End If
    End Sub

    Private Sub UtilitiesButton_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' Only show one utilities form, either open one or center/present one if it is already open
        If Me.isFormTypeAlreadyOpen(UtilitiesForm, True) = False Then
            ' The form is not open - open it!
            Dim newUtilitiesForm As New UtilitiesForm
            newUtilitiesForm.Show()
        End If
    End Sub

    ' Accepts a type and returns true or false if a window of this type already exists
    Function isFormTypeAlreadyOpen(formType As Windows.Forms.Form, shouldPresentToUser As Boolean) As Boolean
        Dim isFormAlreadyOpen = False
        For Each openForm In Application.OpenForms
            If openForm.GetType() Is formType.GetType() Then
                isFormAlreadyOpen = True
                ' Present the user if TRUE was passed
                If shouldPresentToUser Then
                    openForm.WindowState = FormWindowState.Normal
                    openForm.TopMost = True
                End If
            End If
        Next
        Return isFormAlreadyOpen
    End Function
End Class