﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UtilitiesForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GramsTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TroyOuncesTextBox = New System.Windows.Forms.TextBox()
        Me.ConvertButton = New System.Windows.Forms.Button()
        Me.ClearValuesButton = New System.Windows.Forms.Button()
        Me.OuncesConversionButton = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.OuncesTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.platinumTextBox = New System.Windows.Forms.TextBox()
        Me.goldTextBox = New System.Windows.Forms.TextBox()
        Me.silverTextBox = New System.Windows.Forms.TextBox()
        Me.platinumValueButton = New System.Windows.Forms.Button()
        Me.goldCalcButton = New System.Windows.Forms.Button()
        Me.silverCalcButton = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GramsTextBox
        '
        Me.GramsTextBox.Location = New System.Drawing.Point(11, 38)
        Me.GramsTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.GramsTextBox.Name = "GramsTextBox"
        Me.GramsTextBox.Size = New System.Drawing.Size(132, 22)
        Me.GramsTextBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 18)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Grams"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(315, 18)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Troy Ounces"
        '
        'TroyOuncesTextBox
        '
        Me.TroyOuncesTextBox.Location = New System.Drawing.Point(295, 38)
        Me.TroyOuncesTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.TroyOuncesTextBox.Name = "TroyOuncesTextBox"
        Me.TroyOuncesTextBox.Size = New System.Drawing.Size(132, 22)
        Me.TroyOuncesTextBox.TabIndex = 2
        '
        'ConvertButton
        '
        Me.ConvertButton.Location = New System.Drawing.Point(152, 35)
        Me.ConvertButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ConvertButton.Name = "ConvertButton"
        Me.ConvertButton.Size = New System.Drawing.Size(135, 28)
        Me.ConvertButton.TabIndex = 4
        Me.ConvertButton.Text = "<- Convert ->"
        Me.ConvertButton.UseVisualStyleBackColor = True
        '
        'ClearValuesButton
        '
        Me.ClearValuesButton.Location = New System.Drawing.Point(294, 70)
        Me.ClearValuesButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ClearValuesButton.Name = "ClearValuesButton"
        Me.ClearValuesButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ClearValuesButton.Size = New System.Drawing.Size(135, 28)
        Me.ClearValuesButton.TabIndex = 5
        Me.ClearValuesButton.Text = "Clear Values"
        Me.ClearValuesButton.UseVisualStyleBackColor = True
        '
        'OuncesConversionButton
        '
        Me.OuncesConversionButton.Location = New System.Drawing.Point(436, 35)
        Me.OuncesConversionButton.Margin = New System.Windows.Forms.Padding(4)
        Me.OuncesConversionButton.Name = "OuncesConversionButton"
        Me.OuncesConversionButton.Size = New System.Drawing.Size(135, 28)
        Me.OuncesConversionButton.TabIndex = 6
        Me.OuncesConversionButton.Text = "<- Convert ->"
        Me.OuncesConversionButton.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(654, 18)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Ounces"
        '
        'OuncesTextBox
        '
        Me.OuncesTextBox.Location = New System.Drawing.Point(579, 38)
        Me.OuncesTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.OuncesTextBox.Name = "OuncesTextBox"
        Me.OuncesTextBox.Size = New System.Drawing.Size(132, 22)
        Me.OuncesTextBox.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.GramsTextBox)
        Me.GroupBox1.Controls.Add(Me.OuncesTextBox)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.OuncesConversionButton)
        Me.GroupBox1.Controls.Add(Me.TroyOuncesTextBox)
        Me.GroupBox1.Controls.Add(Me.ClearValuesButton)
        Me.GroupBox1.Controls.Add(Me.ConvertButton)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(723, 100)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Conversion Utility"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.silverCalcButton)
        Me.GroupBox2.Controls.Add(Me.goldCalcButton)
        Me.GroupBox2.Controls.Add(Me.platinumValueButton)
        Me.GroupBox2.Controls.Add(Me.silverTextBox)
        Me.GroupBox2.Controls.Add(Me.goldTextBox)
        Me.GroupBox2.Controls.Add(Me.platinumTextBox)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 119)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(722, 236)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Monetary Value Utility"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(434, 17)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Enter the current value for 1 troy ounce of platinum, gold and silver."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 17)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Platinum"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 17)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Gold"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 17)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Silver"
        '
        'platinumTextBox
        '
        Me.platinumTextBox.Location = New System.Drawing.Point(79, 55)
        Me.platinumTextBox.Name = "platinumTextBox"
        Me.platinumTextBox.Size = New System.Drawing.Size(100, 22)
        Me.platinumTextBox.TabIndex = 4
        '
        'goldTextBox
        '
        Me.goldTextBox.Location = New System.Drawing.Point(79, 84)
        Me.goldTextBox.Name = "goldTextBox"
        Me.goldTextBox.Size = New System.Drawing.Size(100, 22)
        Me.goldTextBox.TabIndex = 5
        '
        'silverTextBox
        '
        Me.silverTextBox.Location = New System.Drawing.Point(79, 115)
        Me.silverTextBox.Name = "silverTextBox"
        Me.silverTextBox.Size = New System.Drawing.Size(100, 22)
        Me.silverTextBox.TabIndex = 6
        '
        'platinumValueButton
        '
        Me.platinumValueButton.Location = New System.Drawing.Point(186, 53)
        Me.platinumValueButton.Name = "platinumValueButton"
        Me.platinumValueButton.Size = New System.Drawing.Size(124, 23)
        Me.platinumValueButton.TabIndex = 7
        Me.platinumValueButton.Text = "Calculate"
        Me.platinumValueButton.UseVisualStyleBackColor = True
        '
        'goldCalcButton
        '
        Me.goldCalcButton.Location = New System.Drawing.Point(186, 84)
        Me.goldCalcButton.Name = "goldCalcButton"
        Me.goldCalcButton.Size = New System.Drawing.Size(124, 23)
        Me.goldCalcButton.TabIndex = 8
        Me.goldCalcButton.Text = "Calculate"
        Me.goldCalcButton.UseVisualStyleBackColor = True
        '
        'silverCalcButton
        '
        Me.silverCalcButton.Location = New System.Drawing.Point(186, 115)
        Me.silverCalcButton.Name = "silverCalcButton"
        Me.silverCalcButton.Size = New System.Drawing.Size(124, 23)
        Me.silverCalcButton.TabIndex = 9
        Me.silverCalcButton.Text = "Calculate"
        Me.silverCalcButton.UseVisualStyleBackColor = True
        '
        'UtilitiesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 367)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "UtilitiesForm"
        Me.Text = "Utilities"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GramsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TroyOuncesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConvertButton As System.Windows.Forms.Button
    Friend WithEvents ClearValuesButton As System.Windows.Forms.Button
    Friend WithEvents OuncesConversionButton As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OuncesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents silverTextBox As TextBox
    Friend WithEvents goldTextBox As TextBox
    Friend WithEvents platinumTextBox As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents silverCalcButton As Button
    Friend WithEvents goldCalcButton As Button
    Friend WithEvents platinumValueButton As Button
End Class
