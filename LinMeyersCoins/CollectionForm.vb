﻿Imports System.ComponentModel
Imports LinMeyersCoins.LinMeyersCoinsDBDataSetTableAdapters
Imports LinMeyersCoinsCommon.LinMeyersCoins
' SortableBindingList was not created by me.
' I attempted to use this library and translate it to VB.net but would receive an IndexOutOfRangeException in WinForms.dll
' As the stack trace did not lead to the class I created I was unable to find the problem (Some mistake I made when converting to VB I'm sure)
' Instead I copied the C# into a new library so I can reference it using this VB.net Application
' You may find the code online here: http://stackoverflow.com/questions/1699642/how-to-sort-databound-datagridview-column
Imports SortableBindingList

Public Class CollectionForm
    Private ReadOnly _coins As SortableBindingList(Of Coin) = New SortableBindingList(Of Coin)()

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        ' Do not allow new data to be added and no edits
        CollectionsDataGrid.AllowUserToAddRows = False
        CollectionsDataGrid.ReadOnly = True
        ' Bind the datas to the datagridview
        CollectionsDataGrid.DataSource = Me._coins
        ' Get all the datas
        Me.LoadAllCoinsFromDb()
    End Sub

    Private Sub LoadAllCoinsFromDb()
        ' Open our "connections"
        Using coinTableAdapter = New CoinsTableAdapter,
            coinDataSet = New LinMeyersCoinsDBDataSet
            ' Fill the data 
            coinTableAdapter.Fill(coinDataSet.Coins)
            ' We got out data from the DB - load it into our list of coins to be filtered and changed as needed
            For Each data As DataRow In coinDataSet.Coins
                Dim newCoin = Coin.FromDataRow(data)
                Debug.WriteLine("Got us a new coin: {0}", newCoin.toString())
                Me._coins.Add(newCoin)
            Next
            ' Update the view to display our sweet new data
            CollectionsDataGrid.Update()
            CollectionsDataGrid.Refresh()
        End Using
    End Sub

    Private Sub FilterCoinsByMinimumValue(amount As Decimal)
        ' If the amount is greater than 0 - filter it up
        If amount > 0 Then
            ' Create a new list to serve as the filtered data source
            ' Do not overwrite the _coins variable as it will force us to reload from the DB
            Dim filteredCoins = New SortableBindingList(Of Coin)(Me._coins.Where(Function(c) c.Value >= amount).ToList())
            ' Update the data source for our new filtered list
            CollectionsDataGrid.DataSource = filteredCoins
            ' Resort the grid to show the lowest values (ASCENDING) 
            ' This will display the value closes to the amount they are searching at the top
            CollectionsDataGrid.Sort(CollectionsDataGrid.Columns("Value"), ListSortDirection.Ascending)
        Else
            ' If the amount is 0.. or some how less than 0 clear the filter altogether
            CollectionsDataGrid.DataSource = Me._coins
        End If
        ' Update the Grid
        CollectionsDataGrid.Update()
        CollectionsDataGrid.Refresh()
    End Sub

    ' Handles the data entered into the search box
    Private Sub minTextBox_textChanged(sender As Object, e As EventArgs) Handles minAmountBox.TextChanged
        Dim amount As Decimal = 0.0 ' Default to 0 if the try parse fails we will pass 0 and clear the filter
        Decimal.TryParse(minAmountBox.Text, amount)
        Me.FilterCoinsByMinimumValue(amount)
    End Sub

End Class