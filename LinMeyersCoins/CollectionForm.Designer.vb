﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CollectionForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CollectionsDataGrid = New System.Windows.Forms.DataGridView()
        Me.minAmountBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.CollectionsDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CollectionsDataGrid
        '
        Me.CollectionsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.CollectionsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.CollectionsDataGrid.Location = New System.Drawing.Point(12, 59)
        Me.CollectionsDataGrid.Name = "CollectionsDataGrid"
        Me.CollectionsDataGrid.Size = New System.Drawing.Size(1250, 454)
        Me.CollectionsDataGrid.TabIndex = 0
        '
        'minAmountBox
        '
        Me.minAmountBox.Location = New System.Drawing.Point(88, 16)
        Me.minAmountBox.Name = "minAmountBox"
        Me.minAmountBox.Size = New System.Drawing.Size(152, 20)
        Me.minAmountBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Min. Amount:"
        '
        'CollectionForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1274, 525)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.minAmountBox)
        Me.Controls.Add(Me.CollectionsDataGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "CollectionForm"
        Me.Text = "Your Coins"
        CType(Me.CollectionsDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CollectionsDataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents minAmountBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
