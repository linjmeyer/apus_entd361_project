﻿Imports LinMeyersCoins.LinMeyersCoinsDBDataSetTableAdapters
Imports LinMeyersCoinsCommon.LinMeyersCoins

Public Class NewCoinForm
    Private Sub NewCoinForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Center to screen on open
        Me.CenterToScreen()
    End Sub

    Private Sub NewCoinForm_LocationChange(sender As Object, e As EventArgs) Handles MyBase.LocationChanged
        Me.CenterToScreen()
    End Sub

    Private Sub CoinInsertButton_Click(sender As Object, e As EventArgs) Handles CoinInsertButton.Click
        Me.InsertCoinWithViewValues()
    End Sub

    Private Sub InsertCoinWithViewValues()
        ' They are inserting a coin - grab our values
        Dim coinWeightDecimal As Decimal
        If Decimal.TryParse(CoinWeightTextBox.Text.Trim(), coinWeightDecimal) = False Then
            ' Not a valid number
            Me.invalidDataGiven()
            Return
        End If
        Dim coinMetalType As Coin.MetalType = Me.getMetalTypeFromForm()
        Dim coinCurrencyType As Coin.CurrencyType = Me.getCurrencyTypeFromForm()
        Dim coinNameString = CoinNameTextBox.Text.Trim()
        Dim coinEncapsulated As Boolean = CoinEncapsulatedCheckBox.Checked
        Dim coinYearInt As Integer
        ' Parse the year into an integer, error if we can't
        If Integer.TryParse(CoinYearTextBox.Text.Trim(), coinYearInt) = False Then
            ' Not a valid number
            Me.invalidDataGiven()
            Return
        End If
        ' Get coinFaceValue as a double
        Dim coinFaceValueDecimal As Decimal
        If Decimal.TryParse(CoinFaceValueTextBox.Text.Trim(), coinFaceValueDecimal) = False Then
            ' Not a valid double
            Me.invalidDataGiven()
            Return
        End If
        ' Get coinValue
        Dim coinValueDecimal As Decimal
        If Decimal.TryParse(CoinCollectorValueTextBox.Text.Trim(), coinValueDecimal) = False Then
            ' Not a valid double
            Me.invalidDataGiven()
            Return
        End If

        ' Our numeric values are good, check that all of our strings are not empty
        If coinNameString.Length = 0 Then
            ' One of them is empty
            Me.invalidDataGiven()
            Return
        End If

        ' Everything checks out, make the object
        Dim newCoin = New Coin(coinYearInt, coinNameString, coinWeightDecimal, coinEncapsulated, coinFaceValueDecimal, coinValueDecimal, coinMetalType, coinCurrencyType)
        ' Save to the database, close if successfull
        If Me.SaveCoinToDatabase(newCoin) = True Then
            Me.Close()
        End If
    End Sub

    ' Saves a coin to the local DB
    ' Handles alert messages for success/error
    ' Returns True/False for success
    Private Function SaveCoinToDatabase(newCoin As Coin) As Boolean
        Try
            ' Get our adapter, and insert the values
            Using coinTableAdapter = New CoinsTableAdapter,
            coinDataSet = New LinMeyersCoinsDBDataSet

                ' Be sure to set the enum's to their toString() values so we can parse it back into the proper enum type
                coinTableAdapter.Insert(newCoin.Name, newCoin.Year, newCoin.FaceValue, newCoin.Currency.ToString(), newCoin.Metal.ToString(), newCoin.Weight, newCoin.Value, newCoin.isEncapsulated)

                ' Confirm that it was created with some toString
                MessageBox.Show("You have created a coin: " + Environment.NewLine + newCoin.toString())
                ' Return True to show the requestor that it ws inserted

                ' Debug output the coins in the database to show we are adding them properly
                coinTableAdapter.Fill(coinDataSet.Coins)
                Debug.WriteLine("Displaying Coin Table records ==================================")
                Debug.WriteLine("Current size of Coin DB is... " + coinDataSet.Coins.Count.ToString())
                Dim i = 0
                For Each data As DataRow In coinDataSet.Coins
                    ' Build our Coin object from the DataRow
                    ' Wrap it it's own Try catch so we don't make it seem like it wasn't inserted
                    Try
                        Debug.WriteLine(data.Item("Metal").ToString())
                        Dim dataCoin = Coin.FromDataRow(data)
                        Debug.WriteLine("DB Entry #{0} => {1}", i.ToString(), dataCoin.toString())
                    Catch ex As Exception
                        Debug.WriteLine("Coin was inserted but an exception occured while reading the DB.... " + ex.ToString())
                    End Try
                    i = i + 1
                Next
            End Using
            ' Return true so the caller knows we added it
            Return True
        Catch e As Exception
            ' An error occurred, show the error and return false
            Dim errorString = "There has been an error saving to the database... " + e.ToString()
            MessageBox.Show(errorString)
            Debug.WriteLine(errorString)
            Return False
        End Try
    End Function

    Private Function getMetalTypeFromForm() As Coin.MetalType
        ' Returns the selected radio button type from the group box
        Dim SelectedRadioButton = MetalTypeGroupBox.Controls.OfType(Of RadioButton).FirstOrDefault(Function(r) r.Checked)
        ' Can return null if none selected
        If SelectedRadioButton Is Nothing Then
            Return Coin.MetalType.Other
        End If
        ' We have a valid selection, match it to a MetalType or return Other
        Return Coin.getMetalTypeForString(SelectedRadioButton.Text.Trim())
    End Function

    ' Finds the users select and returns the matching CurrencyType
    Private Function getCurrencyTypeFromForm() As Coin.CurrencyType
        Return Coin.getCurrencyTypeForString(Me.CoinCurrencyComboBox.Text.Trim())
    End Function

    Private Sub CoinCollectorValueTextBox_TextChanged(sender As Object, e As EventArgs) Handles CoinCollectorValueTextBox.TextChanged
        ' When they enter stuff into the text box for the Collector Value, update the label
        coinValueLabel.Text = "Coin is " + Me.getFriendlyCoinValueFromView()
    End Sub

    Private Function getFriendlyCoinValueFromView() As String
        ' Get the value of the form as a string
        Dim coinCollectorValueString = CoinCollectorValueTextBox.Text.Trim()
        ' Try to parse it to a double - default to 0 for no value
        Dim coinCollectorValueDouble As Double = 0
        Dim returnValue = "" ' To be returned, empty for no value yet or invalid value
        If Double.TryParse(coinCollectorValueString, coinCollectorValueDouble) Then
            ' First value should be lower!
            Dim minMaxArray As Double()() = {({0.0, 10.0}), ({10.01, 50.0}), ({50.01, 100.0}), ({100.01, 500.0}), ({500.01, 5000.0}), ({5000.01, Double.MaxValue})}
            ' Parellel array of string values for matching min/max
            Dim valueArray As String() = {"Cheap", "Average", "Above Average", "Valuable", "Very Valuable", "Extremely Valuable"}
            ' Only continue if arrays are same size
            If valueArray.Length = minMaxArray.Length Then
                ' Valid, loop to find our value
                For i As Int32 = 0 To minMaxArray.Length - 1
                    If minMaxArray(i).Length = 2 Then
                        ' Valid minMax, compare the values
                        If coinCollectorValueDouble >= minMaxArray(i)(0) And coinCollectorValueDouble <= minMaxArray(i)(1) Then
                            ' We found the matching value - get it's string from parellel array
                            returnValue = valueArray(i)
                        End If
                    Else
                        ' We hardcoded invalid data, die.
                        Throw New IndexOutOfRangeException("Supplied minMax is too long or too short")
                    End If
                Next
            Else
                ' Values do not match, kill program with exception
                Throw New IndexOutOfRangeException("Supplied valueArray and minMaxArray do not match!")
            End If
        End If
        Return returnValue
    End Function

    Public Sub invalidDataGiven()
        MessageBox.Show("Please provide valid data for each option")
    End Sub
End Class