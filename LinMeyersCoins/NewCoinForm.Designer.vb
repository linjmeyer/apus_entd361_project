﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewCoinForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CoinNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CoinWeightTextBox = New System.Windows.Forms.TextBox()
        Me.CoinInsertButton = New System.Windows.Forms.Button()
        Me.MetalTypeGroupBox = New System.Windows.Forms.GroupBox()
        Me.metalTypeOther = New System.Windows.Forms.RadioButton()
        Me.metalTypePalladium = New System.Windows.Forms.RadioButton()
        Me.metalTypePlatinum = New System.Windows.Forms.RadioButton()
        Me.metalTypeGold = New System.Windows.Forms.RadioButton()
        Me.metalTypeSilver = New System.Windows.Forms.RadioButton()
        Me.metalTypeZync = New System.Windows.Forms.RadioButton()
        Me.metalTypeCopper = New System.Windows.Forms.RadioButton()
        Me.CoinCurrencyComboBox = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CoinFaceValueTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CoinCollectorValueTextBox = New System.Windows.Forms.TextBox()
        Me.CoinYearTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CoinEncapsulatedCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.coinValueLabel = New System.Windows.Forms.Label()
        Me.MetalTypeGroupBox.SuspendLayout
        Me.SuspendLayout
        '
        'CoinNameTextBox
        '
        Me.CoinNameTextBox.Location = New System.Drawing.Point(119, 5)
        Me.CoinNameTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinNameTextBox.Name = "CoinNameTextBox"
        Me.CoinNameTextBox.Size = New System.Drawing.Size(126, 20)
        Me.CoinNameTextBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(9, 7)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Coin Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(9, 31)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Coin Weight"
        '
        'CoinWeightTextBox
        '
        Me.CoinWeightTextBox.Location = New System.Drawing.Point(119, 28)
        Me.CoinWeightTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinWeightTextBox.Name = "CoinWeightTextBox"
        Me.CoinWeightTextBox.Size = New System.Drawing.Size(126, 20)
        Me.CoinWeightTextBox.TabIndex = 2
        '
        'CoinInsertButton
        '
        Me.CoinInsertButton.Location = New System.Drawing.Point(364, 396)
        Me.CoinInsertButton.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinInsertButton.Name = "CoinInsertButton"
        Me.CoinInsertButton.Size = New System.Drawing.Size(124, 19)
        Me.CoinInsertButton.TabIndex = 4
        Me.CoinInsertButton.Text = "Insert Coin"
        Me.CoinInsertButton.UseVisualStyleBackColor = true
        '
        'MetalTypeGroupBox
        '
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypeOther)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypePalladium)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypePlatinum)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypeGold)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypeSilver)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypeZync)
        Me.MetalTypeGroupBox.Controls.Add(Me.metalTypeCopper)
        Me.MetalTypeGroupBox.Location = New System.Drawing.Point(11, 60)
        Me.MetalTypeGroupBox.Margin = New System.Windows.Forms.Padding(2)
        Me.MetalTypeGroupBox.Name = "MetalTypeGroupBox"
        Me.MetalTypeGroupBox.Padding = New System.Windows.Forms.Padding(2)
        Me.MetalTypeGroupBox.Size = New System.Drawing.Size(232, 181)
        Me.MetalTypeGroupBox.TabIndex = 5
        Me.MetalTypeGroupBox.TabStop = false
        Me.MetalTypeGroupBox.Text = "Metal Type"
        '
        'metalTypeOther
        '
        Me.metalTypeOther.AutoSize = true
        Me.metalTypeOther.Location = New System.Drawing.Point(4, 149)
        Me.metalTypeOther.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypeOther.Name = "metalTypeOther"
        Me.metalTypeOther.Size = New System.Drawing.Size(51, 17)
        Me.metalTypeOther.TabIndex = 6
        Me.metalTypeOther.TabStop = true
        Me.metalTypeOther.Text = "Other"
        Me.metalTypeOther.UseVisualStyleBackColor = true
        '
        'metalTypePalladium
        '
        Me.metalTypePalladium.AutoSize = true
        Me.metalTypePalladium.Location = New System.Drawing.Point(4, 127)
        Me.metalTypePalladium.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypePalladium.Name = "metalTypePalladium"
        Me.metalTypePalladium.Size = New System.Drawing.Size(70, 17)
        Me.metalTypePalladium.TabIndex = 5
        Me.metalTypePalladium.TabStop = true
        Me.metalTypePalladium.Text = "Palladium"
        Me.metalTypePalladium.UseVisualStyleBackColor = true
        '
        'metalTypePlatinum
        '
        Me.metalTypePlatinum.AutoSize = true
        Me.metalTypePlatinum.Location = New System.Drawing.Point(4, 105)
        Me.metalTypePlatinum.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypePlatinum.Name = "metalTypePlatinum"
        Me.metalTypePlatinum.Size = New System.Drawing.Size(65, 17)
        Me.metalTypePlatinum.TabIndex = 4
        Me.metalTypePlatinum.TabStop = true
        Me.metalTypePlatinum.Text = "Platinum"
        Me.metalTypePlatinum.UseVisualStyleBackColor = true
        '
        'metalTypeGold
        '
        Me.metalTypeGold.AutoSize = true
        Me.metalTypeGold.Location = New System.Drawing.Point(4, 83)
        Me.metalTypeGold.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypeGold.Name = "metalTypeGold"
        Me.metalTypeGold.Size = New System.Drawing.Size(47, 17)
        Me.metalTypeGold.TabIndex = 3
        Me.metalTypeGold.TabStop = true
        Me.metalTypeGold.Text = "Gold"
        Me.metalTypeGold.UseVisualStyleBackColor = true
        '
        'metalTypeSilver
        '
        Me.metalTypeSilver.AutoSize = true
        Me.metalTypeSilver.Location = New System.Drawing.Point(4, 61)
        Me.metalTypeSilver.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypeSilver.Name = "metalTypeSilver"
        Me.metalTypeSilver.Size = New System.Drawing.Size(51, 17)
        Me.metalTypeSilver.TabIndex = 2
        Me.metalTypeSilver.TabStop = true
        Me.metalTypeSilver.Text = "Silver"
        Me.metalTypeSilver.UseVisualStyleBackColor = true
        '
        'metalTypeZync
        '
        Me.metalTypeZync.AutoSize = true
        Me.metalTypeZync.Location = New System.Drawing.Point(4, 39)
        Me.metalTypeZync.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypeZync.Name = "metalTypeZync"
        Me.metalTypeZync.Size = New System.Drawing.Size(46, 17)
        Me.metalTypeZync.TabIndex = 1
        Me.metalTypeZync.TabStop = true
        Me.metalTypeZync.Text = "Zinc"
        Me.metalTypeZync.UseVisualStyleBackColor = true
        '
        'metalTypeCopper
        '
        Me.metalTypeCopper.AutoSize = true
        Me.metalTypeCopper.Location = New System.Drawing.Point(4, 17)
        Me.metalTypeCopper.Margin = New System.Windows.Forms.Padding(2)
        Me.metalTypeCopper.Name = "metalTypeCopper"
        Me.metalTypeCopper.Size = New System.Drawing.Size(59, 17)
        Me.metalTypeCopper.TabIndex = 0
        Me.metalTypeCopper.TabStop = true
        Me.metalTypeCopper.Text = "Copper"
        Me.metalTypeCopper.UseVisualStyleBackColor = true
        '
        'CoinCurrencyComboBox
        '
        Me.CoinCurrencyComboBox.FormattingEnabled = true
        Me.CoinCurrencyComboBox.Items.AddRange(New Object() {"USD", "EUR", "GBP"})
        Me.CoinCurrencyComboBox.Location = New System.Drawing.Point(338, 5)
        Me.CoinCurrencyComboBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinCurrencyComboBox.Name = "CoinCurrencyComboBox"
        Me.CoinCurrencyComboBox.Size = New System.Drawing.Size(126, 21)
        Me.CoinCurrencyComboBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(257, 7)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Currency"
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(257, 32)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Face Value"
        '
        'CoinFaceValueTextBox
        '
        Me.CoinFaceValueTextBox.Location = New System.Drawing.Point(338, 30)
        Me.CoinFaceValueTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinFaceValueTextBox.Name = "CoinFaceValueTextBox"
        Me.CoinFaceValueTextBox.Size = New System.Drawing.Size(126, 20)
        Me.CoinFaceValueTextBox.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(257, 55)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Collector Value"
        '
        'CoinCollectorValueTextBox
        '
        Me.CoinCollectorValueTextBox.Location = New System.Drawing.Point(338, 53)
        Me.CoinCollectorValueTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinCollectorValueTextBox.Name = "CoinCollectorValueTextBox"
        Me.CoinCollectorValueTextBox.Size = New System.Drawing.Size(126, 20)
        Me.CoinCollectorValueTextBox.TabIndex = 10
        '
        'CoinYearTextBox
        '
        Me.CoinYearTextBox.Location = New System.Drawing.Point(338, 101)
        Me.CoinYearTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinYearTextBox.Name = "CoinYearTextBox"
        Me.CoinYearTextBox.Size = New System.Drawing.Size(126, 20)
        Me.CoinYearTextBox.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(257, 103)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Coin Year"
        '
        'CoinEncapsulatedCheckBox
        '
        Me.CoinEncapsulatedCheckBox.AutoSize = True
        Me.CoinEncapsulatedCheckBox.Location = New System.Drawing.Point(449, 123)
        Me.CoinEncapsulatedCheckBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CoinEncapsulatedCheckBox.Name = "CoinEncapsulatedCheckBox"
        Me.CoinEncapsulatedCheckBox.Size = New System.Drawing.Size(15, 14)
        Me.CoinEncapsulatedCheckBox.TabIndex = 14
        Me.CoinEncapsulatedCheckBox.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(257, 123)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Coin is Encapsulated"
        '
        'coinValueLabel
        '
        Me.coinValueLabel.AutoSize = True
        Me.coinValueLabel.Location = New System.Drawing.Point(257, 79)
        Me.coinValueLabel.Name = "coinValueLabel"
        Me.coinValueLabel.Size = New System.Drawing.Size(51, 13)
        Me.coinValueLabel.TabIndex = 16
        Me.coinValueLabel.Text = "Coin Is...."
        '
        'NewCoinForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(498, 425)
        Me.Controls.Add(Me.coinValueLabel)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.CoinEncapsulatedCheckBox)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.CoinYearTextBox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.CoinCollectorValueTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CoinFaceValueTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CoinCurrencyComboBox)
        Me.Controls.Add(Me.MetalTypeGroupBox)
        Me.Controls.Add(Me.CoinInsertButton)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CoinWeightTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CoinNameTextBox)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "NewCoinForm"
        Me.ShowInTaskbar = false
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Insert a Coin!"
        Me.TopMost = true
        Me.MetalTypeGroupBox.ResumeLayout(false)
        Me.MetalTypeGroupBox.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents CoinNameTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents CoinWeightTextBox As TextBox
    Friend WithEvents CoinInsertButton As Button
    Friend WithEvents MetalTypeGroupBox As GroupBox
    Friend WithEvents metalTypeZync As RadioButton
    Friend WithEvents metalTypeCopper As RadioButton
    Friend WithEvents metalTypeOther As RadioButton
    Friend WithEvents metalTypePalladium As RadioButton
    Friend WithEvents metalTypePlatinum As RadioButton
    Friend WithEvents metalTypeGold As RadioButton
    Friend WithEvents metalTypeSilver As RadioButton
    Friend WithEvents CoinCurrencyComboBox As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents CoinFaceValueTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents CoinCollectorValueTextBox As TextBox
    Friend WithEvents CoinYearTextBox As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents CoinEncapsulatedCheckBox As CheckBox
    Friend WithEvents Label7 As Label
    Friend WithEvents coinValueLabel As System.Windows.Forms.Label
End Class
