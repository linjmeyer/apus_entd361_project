﻿Imports System.Text

Namespace LinMeyersCoins
    Public Class Coin
        Private _Year As Integer
        Public Property Year() As Integer
            Get
                Return _Year
            End Get
            Set(ByVal value As Integer)
                _Year = value
            End Set
        End Property

        Private _Name As String
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal value As String)
                _Name = value
            End Set
        End Property

        Private _Weight As Decimal
        Public Property Weight() As Decimal
            Get
                Return _Weight
            End Get
            Set(ByVal value As Decimal)
                _Weight = value
            End Set
        End Property

        Private _Encapsulated As Boolean
        Public Property isEncapsulated() As Boolean
            Get
                Return _Encapsulated
            End Get
            Set(ByVal value As Boolean)
                _Encapsulated = value
            End Set
        End Property

        Private _FaceValue As Decimal
        Public Property FaceValue() As Decimal
            Get
                Return _FaceValue
            End Get
            Set(ByVal value As Decimal)
                _FaceValue = value
            End Set
        End Property

        Private _Value As Decimal
        Public Property Value() As Decimal
            Get
                Return _Value
            End Get
            Set(ByVal value As Decimal)
                _Value = value
            End Set
        End Property

        Public Enum MetalType
            Copper
            Zinc
            Silver
            Gold
            Platinum
            Palladium
            Other
        End Enum

        Private _Metal As MetalType = MetalType.Other
        Public Property Metal() As MetalType
            Get
                Return _Metal
            End Get
            Set(ByVal value As MetalType)
                _Metal = value
            End Set
        End Property

        Public Enum CurrencyType
            USD
            EUR
            GBP
            Other
        End Enum

        Private _Currency As CurrencyType = CurrencyType.Other
        Public Property Currency() As CurrencyType
            Get
                Return _Currency
            End Get
            Set(ByVal value As CurrencyType)
                _Currency = value
            End Set
        End Property

        ' Constructor requires all fields except Enumerables
        Sub New(year As Integer, name As String, weight As Decimal, encapped As Boolean, faceValue As Decimal, value As Decimal, mType As MetalType, curType As CurrencyType)
            ' Set our values
            Me.Year = year
            Me.Name = name
            Me.Weight = weight
            Me.FaceValue = faceValue
            Me.Value = value
            Me.isEncapsulated = encapped
            Me.Metal = mType
            Me.Currency = curType
        End Sub

        ' Override toString
        Overrides Function toString() As String
            Dim builder As New StringBuilder()
            builder.Append("Name => ")
            builder.Append(Me.Name)
            builder.Append(", Weight => ")
            builder.Append(Me.Weight)
            builder.Append(", Value => ")
            builder.Append(Me.Value)
            builder.Append(", FaceValue => ")
            builder.Append(Me.FaceValue)
            builder.Append(", Year => ")
            builder.Append(Me.Year)
            builder.Append(", Metal => ")
            builder.Append(Me.Metal.ToString())
            builder.Append(", Currency => ")
            builder.Append(Me.Currency.ToString())
            builder.Append(", Is Encapsulated => ")
            builder.Append(Me.isEncapsulated.ToString())
            Return builder.ToString()
        End Function



        ' Some static helper functions
        ' Gets a metal type for a string value, defaults to Other
        Public Shared Function getMetalTypeForString(mType As String) As MetalType
            Select Case mType
                Case "Copper", 0
                    Return Coin.MetalType.Copper
                Case "Zinc", 1
                    Return Coin.MetalType.Zinc
                Case "Silver", 2
                    Return Coin.MetalType.Silver
                Case "Gold", 3
                    Return Coin.MetalType.Gold
                Case "Platinum", 4
                    Return Coin.MetalType.Platinum
                Case "Palladium", 5
                    Return Coin.MetalType.Palladium
                Case Else
                    Return Coin.MetalType.Other
            End Select
        End Function

        ' Gets a currency type for a string value, default to Other
        Public Shared Function getCurrencyTypeForString(curType As String) As CurrencyType
            Select Case curType
                Case "USD", 0
                    Return Coin.CurrencyType.USD
                Case "EUR", 1
                    Return Coin.CurrencyType.EUR
                Case "GPB", 2
                    Return Coin.CurrencyType.GBP
                Case Else
                    Return Coin.CurrencyType.Other
            End Select
        End Function

        ' Autobuilder from DataRow type fetched from DB
        ' Will throw exception so should be caught by caller
        Public Shared Function FromDataRow(data As DataRow) As Coin
            Dim year = CType(data.Item("Year"), Integer)
            Dim name = CType(data.Item("Name"), String)
            Dim weight = CType(data.Item("Weight"), Decimal)
            Dim faceValue = CType(data.Item("FaceValue"), Decimal)
            Dim value = CType(data.Item("Value"), Decimal)
            Dim isEncap = CType(data.Item("IsEncapsulated"), Boolean)
            Dim metal = Coin.getMetalTypeForString(CType(data.Item("Metal"), String))
            Dim currency = Coin.getCurrencyTypeForString(CType(data.Item("Currency"), String))
            Return New Coin(year, name, weight, isEncap, faceValue, value, metal, currency)
        End Function
    End Class
End Namespace

